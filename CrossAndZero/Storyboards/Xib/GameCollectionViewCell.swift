//
//  GameCollectionViewCell.swift
//  CrossAndZero
//
//  Created by Евгений on 26/02/2019.
//  Copyright © 2019 Evgeniy. All rights reserved.
//

import UIKit

class GameCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var gameImageView: UIImageView! {
        didSet {
            // REVIEW: У ячеек нет метода viewDidLoad, поэтому инициализацию можно делать в методе awakeFromNib,
            // если ты загружаешь ее из ксиба
            gameImageView.backgroundColor = UIColor(red: 235 / 255, green: 242 / 255, blue: 255 / 255, alpha: 1)
        }
    }
    
    
    
    func setupCell(indexPath: IndexPath) {
        
        
    }
    
     
    

}
