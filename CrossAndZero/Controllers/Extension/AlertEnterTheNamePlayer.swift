//
//  UIAlertController.swift
//  CrossAndZero
//
//  Created by Евгений on 20/02/2019.
//  Copyright © 2019 Evgeniy. All rights reserved.
//

import UIKit

extension UIViewController {
    
    // REVIEW:
    //
    // В функции происходит два одинаковых действия, которые правильно было бы вынести в отдельную функцию
    //
    // Сейчас твоя функция делает следующее:
    //
    // Показать алерт1 с заголовком1 и текстом1
    // При нажатии на кнопку ок взять значение алерт1.текстфилда и
    //
    //    сохранить значение алерт1.текстфилда в GameModel.shared.userOne.nameUser
    //
    //    Показать алерт2 с заголовком2 и текстом2
    //    При нажатии на кнопку ок взять значение алерт2.текстфилда и
    //
    //        сохранить значение алерт1.текстфилда в GameModel.shared.userTwo.nameUser
    //
    // Чтобы вынести повторяющийся код нужно создать функцию со следующим интерфейсом
    //
    // func requestStringWith(title: String, message: String, completion: @escaping (String) -> ())
    //
    // Эта функция должна показать алерт с тайтлом title, сообщением message и вызвать completion блок когда нажата кнопка ок

    
    func alert(name: UILabel) {
        let alertOnePlayer = UIAlertController(title: "Введите имя", message: "Игрок № 1", preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "Ок", style: .cancel) { (text) in // REVIEW: то что ты здесь назвал text на самом деле объект типа UIAlertAction, лучше переназови, чтобы потом не запутаться

            let alertTwoPlayer = UIAlertController(title: "Введите имя", message: "Игрок № 2", preferredStyle: .alert)
            let alertActionTwoPlayer = UIAlertAction(title: "Ок", style: .default, handler: { (text) in // REVIEW: см предыдущий коммент
                
                if let textField = alertTwoPlayer.textFields {
                    GameModel.shared.userTwo.nameUser = GameModel.shared.userTwo.getName(text: textField.first?.text ?? "O")
                }
            })
            
            alertTwoPlayer.addAction(alertActionTwoPlayer)
            alertTwoPlayer.addTextField { (text) in
                text.placeholder = "Имя"
            }
            self.present(alertTwoPlayer, animated: true, completion: nil)
            
            if let textField = alertOnePlayer.textFields {
                GameModel.shared.userOne.nameUser = GameModel.shared.userOne.getName(text: textField.first?.text ?? "O")
                name.text = textField.first?.text
            }
            
        }
        alertOnePlayer.addAction(alertAction)
        alertOnePlayer.addTextField { (text) in
            text.placeholder = "Имя"
        }
        self.present(alertOnePlayer, animated: true, completion: nil)
    }
}
