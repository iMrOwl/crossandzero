//
//  UIColor.swift
//  CrossAndZero
//
//  Created by Евгений on 18/02/2019.
//  Copyright © 2019 Evgeniy. All rights reserved.
//

import UIKit

extension UIColor {
    
    /// Цвет навигейшен бара и кнопки выхода
    class var scampi: UIColor {
        return UIColor(red: 94 / 255, green: 93 / 255, blue:  163 / 255, alpha: 1)
    }
    
    /// Цвет счетчика победы нолика
    class var mantis: UIColor {
        return UIColor(red: 122 / 255, green: 194 / 255, blue: 88 / 255, alpha: 1)
    }
    
    /// Цвет счетчика победы крестика
    class var cinnabar: UIColor {
        return UIColor(red: 235 / 255, green: 76 / 255, blue: 59 / 255, alpha: 1)
    }
    
    /// Цвет начального значения счетчика победы
    class var silver: UIColor {
        return UIColor(red: 196 / 255, green: 196 / 255, blue: 196 / 255, alpha: 1)
    }
    
    /// Цвет заднего фона вью
    class var zumthor: UIColor {
        return UIColor(red: 235 / 255, green: 242 / 255, blue: 255 / 255, alpha: 1)
    }
    
    /// Цвет заднего фона игрового поля
    class var dustyGray: UIColor {
        return UIColor(red: 151 / 255, green: 151 / 255, blue: 151 / 255, alpha: 1)
    }
    
    /// Цвет крестика
    class var mediumRedViolet: UIColor {
         return UIColor(red: 183 / 255, green: 37 / 255, blue: 159 / 255, alpha: 1)
    }
    
    /// Цвет нолика
    class var horizon: UIColor {
        return UIColor(red: 93 / 255, green: 123 / 255, blue: 163 / 255, alpha: 1)
    }
}


