//
//  String.swift
//  CrossAndZero
//
//  Created by Евгений on 18/02/2019.
//  Copyright © 2019 Evgeniy. All rights reserved.
//

import UIKit

extension String {
    
    /// Выход
    static var exit: String {
        return "Выход"
    }
    
    /// Ход игрока Один
    static var walksUser: String {
        return "Ходит игрок "
    }
    
    /// Название шрифта
    static var robotoRegular: String {
        return "Roboto-Regular"
    }
    
    /// Название сегвея
    static var transition: String {
        return "GoResult"
    }
    
    /// Нил
    static var nile: String {
        return "nil"
    }
    
    /// Крестик
    static var cross: String {
        return "cross"
    }
    
    /// Нолик
    static var zero: String {
        return "zero"
    }
    
    /// Картинка креста в игре
    static var imageCrossGame: String {
        return "cross_game"
    }
    
    /// Картинка круга в игре
    static var imageEllipseGame: String {
        return  "ellipse_game"
    }
    
    static var gameCollectionViewCellId: String {
        return "GameCollectionViewCell"
    }
}
