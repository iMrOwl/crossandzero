//
//  Users.swift
//  CrossAndZero
//
//  Created by Евгений on 05/03/2019.
//  Copyright © 2019 Evgeniy. All rights reserved.
//

protocol User {
    
    var nameUser: String { get set}
    var arrayPlayerMove: [Int] { get set }
    var commonElement: [Int] { get set }
    
    func getName(text: String) -> String
}
