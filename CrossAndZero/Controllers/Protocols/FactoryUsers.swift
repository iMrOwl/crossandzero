//
//  FactoryUsers.swift
//  CrossAndZero
//
//  Created by Евгений on 05/03/2019.
//  Copyright © 2019 Evgeniy. All rights reserved.
//

import UIKit

enum Users {
    case userOne
    case userTwo
    case userComputer
}

class FactoryUsers {
    
    // REVIEW: Опечатка, shared
    static let sharet = FactoryUsers()
    
    func createUsers(nameEnum: Users) -> User {
        switch nameEnum {
        case .userOne:
            return UserOne()
        case .userTwo:
            return UserTwo()
        case .userComputer:
            return UserComputer()
        }
    }
}
