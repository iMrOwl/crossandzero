//
//  ViewController.swift
//  CrossAndZero
//
//  Created by Евгений on 18/02/2019.
//  Copyright © 2019 Evgeniy. All rights reserved.
//

import UIKit

class GameViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    @IBOutlet weak var resultStackView: UIStackView!
    @IBOutlet var resultViewCollection: [UIView]!
    @IBOutlet weak var whoseTurnLable: UILabel!
    @IBOutlet weak var gameCollectionView: UICollectionView! {
        didSet {
            gameCollectionView.backgroundColor = .silver // REVIEW: Почему цвет фона ты задаешь здесь а все остальное делаешь во viewDidLoad? Судя по коду gameCollectionView задается только один раз при загрузке контроллера, поэтому нет смысла отслеживать новые значения
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        gameCollectionView.delegate = self
        gameCollectionView.dataSource = self
        
        whoseTurnLable.text = .walksUser + (GameModel.shared.userOne.nameUser)
        let nibXib = UINib(nibName: .gameCollectionViewCellId ,bundle: nil)
        gameCollectionView.register(nibXib, forCellWithReuseIdentifier: .gameCollectionViewCellId)
        
        alert(name: whoseTurnLable)
        GameViewController.defaultSettings(view: view, navigationController: navigationController!)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        GameModel.shared.returnInitialState.reloadBackView(resultViewCollection: resultViewCollection)
    }
    
    @IBAction func closeActionButton(_ sender: UIButton) {
        exit(0) // REVIEW: Для этого проекта ок, но вообще надо всегда помнить что такое в стор не пропустят
    }
    
    // REVIEW: Соответствие протоколам лучше выносить в отдельный extension, так нагляднее + это видно в навигации
    // Еще можно добавлять
    // MARK: - UICollectionViewDelegate
    // Если нажмешь ctrl + 6 увидишь полоску и текст

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return ModelMove.shared.dictionaryImage.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: .gameCollectionViewCellId, for: indexPath) as! GameCollectionViewCell
        
        cell.gameImageView.image = UIImage(named: ModelMove.shared.dictionaryImage[indexPath.row])
        cell.isUserInteractionEnabled = ModelMove.shared.dictionaryState[indexPath.row]

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        gameAction(indexPath)
        gameCollectionView.reloadData()
        // REVIEW: Перезагрузка collectionView - это результат игрового действия, а не нажатия на ячейку, поэтому лучше перезагрузку делать в методе gameAction
        // Подумай, может быть достаточно обновлять только одну ячейку, вместо обновления всего collectionView
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        // REVIEW: Что за число такое 3.01?
        return CGSize(width: gameCollectionView.layer.frame.width / 3.01, height: gameCollectionView.layer.frame.height / 3.01)
    }
    
    private func gameAction(_ indexPathRow: IndexPath) {
        GameModel.shared.move.toggle()
        GameModel.shared.setImage.setImage(indexPath: indexPathRow, move: GameModel.shared.move, walksLable: whoseTurnLable)
        GameModel.shared.winCheck.winCheck(indexPath: indexPathRow, whoseTurnLable: whoseTurnLable, resultStackView: resultStackView)
        
        if GameModel.shared.goResultViewConstanta {
            go()
        }
    }
    
    private func go() {
        let resultViewController = ResultViewController()
        
        // REVIEW: Перенеси подсчет результата в GameModel
        // Результат сделай enum
        if GameModel.shared.winZero > GameModel.shared.winCross {
            resultViewController.config(resultCountTrans: 1)
        } else {
            resultViewController.config(resultCountTrans: 0)
        }
        // REVIEW: Устанавливаешь modalPresentationStyle, а потом делаешь push, для чего?
        // modalPresentationStyle работает только если делаешь present
        
        resultViewController.modalPresentationStyle = .overCurrentContext
        
        self.navigationController?.pushViewController(resultViewController, animated: true)
    }
}
