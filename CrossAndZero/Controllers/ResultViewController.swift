//
//  ResultViewController.swift
//  CrossAndZero
//
//  Created by Евгений on 18/02/2019.
//  Copyright © 2019 Evgeniy. All rights reserved.
//

import UIKit

class ResultViewController: UIViewController {
    
    @IBOutlet weak var resultImageView: UIImageView!
    var resultCount: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Скрывает кнопку назад
        let backBatton = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: navigationController, action: nil)
        navigationItem.leftBarButtonItem = backBatton
        // Уход с основного экрана через 4 секудны
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(4), execute: {
             self.navigationController?.popViewController(animated: true)
        })
        loadImageOne()

        //Ecтановка фона
        ResultViewController.defaultSettings(view: view, navigationController: navigationController!)
    }

    // Загрузка изображения
    func loadImageOne() {
        if resultCount == 1 {
            resultImageView.image = UIImage(named: "cross_result")
        } else {
            resultImageView.image = UIImage(named: "ellipse_result")
        }
    }
    
    func config(resultCountTrans: Int) {
        self.resultCount = resultCountTrans
    }
}
