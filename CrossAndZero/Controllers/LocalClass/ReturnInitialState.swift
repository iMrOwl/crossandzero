//
//  ResetState.swift
//  CrossAndZero
//
//  Created by Евгений on 19/02/2019.
//  Copyright © 2019 Evgeniy. All rights reserved.
//

import UIKit

// Возвращение к исходному состоянию
class ReturnInitialState {
    
    // Перезапуск счетчиков победы
     func reloadStateViewCollection(resultViewCollection : [UIView]) {
    
        for index in resultViewCollection {
            index.backgroundColor = UIColor.silver
        }
    }
    
    // Перезапуск моделей данных
     func reloadModels(lable: UILabel) {
        
        // REVIEW: Вынеси GameModel.shared в поле класса, чтобы можно было писать gameModel.countMoves = 0
        // Это существенно упростит понимание когда другими разработчиками
        
        // REVIEW: Все что происходит с GameModel перенеси в GameModel
        GameModel.shared.countMoves = 0
        GameModel.shared.move = true
        lable.text = .walksUser + GameModel.shared.userOne.nameUser
        print(GameModel.shared.countCross)
        GameModel.shared.userOne.arrayPlayerMove.removeAll()
        GameModel.shared.userTwo.arrayPlayerMove.removeAll()
        GameModel.shared.userOne.commonElement.removeAll()
        GameModel.shared.userTwo.commonElement.removeAll()
    }
    
    // Перезагрузка в начально состояние
     func reloadBackView(resultViewCollection: [UIView]) {
        // REVIEW: Действие только с ModelMove, перенеси в ModelMove
        ModelMove.shared.dictionaryImage =  ModelMove.shared.defaulDictionaryImage
        
        // REVIEW: Все что касается gameModel перенеси в GameModel
        GameModel.shared.returnInitialState.reloadStateViewCollection(resultViewCollection: resultViewCollection)
        GameModel.shared.countCross = 0
        GameModel.shared.countZero = 7
        GameModel.shared.goResultViewConstanta = false
    }
}
