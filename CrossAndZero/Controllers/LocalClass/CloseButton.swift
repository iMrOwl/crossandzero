//
//  CloseButton.swift
//  CrossAndZero
//
//  Created by Евгений on 18/02/2019.
//  Copyright © 2019 Evgeniy. All rights reserved.
//

import UIKit

class CloseButton: UIButton {
    
    //Инициализация кнопки и ее свойств
    required init?(coder aDecoder: NSCoder) {
      
        super.init(coder: aDecoder)
        
        backgroundColor = .scampi
        tintColor = .white
        setTitle(.exit, for: .normal)
        titleLabel?.font = UIFont(name: .robotoRegular, size: 24)
    }
}
