//
//  UILable.swift
//  CrossAndZero
//
//  Created by Евгений on 18/02/2019.
//  Copyright © 2019 Evgeniy. All rights reserved.
//

import UIKit

class WalksLable: UILabel {
    
    //Настройка лейбла ходов
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        font = UIFont(name: .robotoRegular, size: 24)
        text = " "
        textAlignment = .center
    }
    
}
