//
//  SetImage.swift
//  CrossAndZero
//
//  Created by Евгений on 19/02/2019.
//  Copyright © 2019 Evgeniy. All rights reserved.
//

import UIKit

class SetImage {
    
    //Установка картинки
     func setImage(indexPath: IndexPath,move: Bool, walksLable: UILabel) {
        
        if move {
            ModelMove.shared.dictionaryImage[indexPath.row] = String.imageCrossGame
            ModelMove.shared.dictionaryState[indexPath.row] = false
            walksLable.text = .walksUser + GameModel.shared.userOne.nameUser
        } else {
            ModelMove.shared.dictionaryImage[indexPath.row] = String.imageEllipseGame
            ModelMove.shared.dictionaryState[indexPath.row] = false
            walksLable.text = .walksUser + GameModel.shared.userTwo.nameUser
        }
    }
}
