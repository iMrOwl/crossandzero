//
//  GameButton.swift
//  CrossAndZero
//
//  Created by Евгений on 18/02/2019.
//  Copyright © 2019 Evgeniy. All rights reserved.
//

import UIKit

class GameButton: UIButton{
    
    //Инициализация игровых кнопок и их настройка
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        backgroundColor = .zumthor
        setTitle(nil, for: .normal)
    }
}
