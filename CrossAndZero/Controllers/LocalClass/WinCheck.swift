//
//  WinCheck.swift
//  CrossAndZero
//
//  Created by Евгений on 22/02/2019.
//  Copyright © 2019 Evgeniy. All rights reserved.
//

import UIKit
import Foundation

class WinCheck {
    
     func winCheck(indexPath: IndexPath, whoseTurnLable: UILabel, resultStackView: UIStackView) {
        
        // REVIEW: все синглтоны вынеси в поля класса

        if GameModel.shared.countMoves % 2 == 0 {
            GameModel.shared.userOne.arrayPlayerMove.append(indexPath.row)
            WinnCombinations.shared.arraySaveUserOneWalk.append(indexPath.row)
        } else {
            GameModel.shared.userTwo.arrayPlayerMove.append(indexPath.row)
            WinnCombinations.shared.arraySaveUserTwoWalk.append(indexPath.row)
        }

        GameModel.shared.countMoves += 1
        if GameModel.shared.countMoves < 8 {
        
        WinnCombinations.shared.arraySaveAllWalk.append(indexPath.row)
        
            
        for index in WinnCombinations.shared.winnCombinations {
            
            GameModel.shared.userOne.commonElement = index.filter(GameModel.shared.userOne.arrayPlayerMove.contains)
            GameModel.shared.userTwo.commonElement = index.filter(GameModel.shared.userTwo.arrayPlayerMove.contains)
        
                if index == GameModel.shared.userOne.commonElement {
                    GameModel.shared.staticCount = true
                    
                    // REVIEW: Создаешь контроллер и потом его не используешь
                    CountWin.countWin(stackView: resultStackView, newViewController: GameViewController())
                    
                    GameModel.shared.returnInitialState.reloadModels(lable: whoseTurnLable)
                    ModelMove.shared.dictionaryImage = ModelMove.shared.defaulDictionaryImage
                    ModelMove.shared.dictionaryState = ModelMove.shared.defaulDictionaryState
                    break
                } else if index == GameModel.shared.userTwo.commonElement {
                    GameModel.shared.staticCount = false
                    CountWin.countWin(stackView: resultStackView, newViewController: GameViewController())
                    GameModel.shared.returnInitialState.reloadModels(lable: whoseTurnLable)
                    ModelMove.shared.dictionaryImage = ModelMove.shared.defaulDictionaryImage
                    ModelMove.shared.dictionaryState = ModelMove.shared.defaulDictionaryState
                    break
                }
            }
            } else {
                    ModelMove.shared.dictionaryImage = ModelMove.shared.defaulDictionaryImage
                    ModelMove.shared.dictionaryState = ModelMove.shared.defaulDictionaryState
                    GameModel.shared.returnInitialState.reloadModels(lable: whoseTurnLable)
                }
            }
        }
