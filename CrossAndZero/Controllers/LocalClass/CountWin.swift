//
//  CountWin.swift
//  CrossAndZero
//
//  Created by Евгений on 22/02/2019.
//  Copyright © 2019 Evgeniy. All rights reserved.
//

import UIKit

class CountWin {
    
    //Определнеление цвета победителя
    static func countWin(stackView: UIStackView, newViewController: UIViewController) {
        if GameModel.shared.countZero != GameModel.shared.countCross {
            if GameModel.shared.staticCount {
                let buttonGtay = stackView.arrangedSubviews[GameModel.shared.countCross]
                buttonGtay.backgroundColor = .green
                GameModel.shared.countCross += 1
                GameModel.shared.winCross += 1
                
                
                // REVIEW: Достаточно написать просто else
                // если нужно узнать что bool == false, лучше использовать логическое отрицание !bool
            } else if GameModel.shared.staticCount == false  {
                let buttonGtay = stackView.arrangedSubviews[GameModel.shared.countZero]
                buttonGtay.backgroundColor = .red
                GameModel.shared.countZero -= 1
                GameModel.shared.winZero += 1
            }
        } else {
            // REVIEW: Если константа, то почему она меняется?
            GameModel.shared.goResultViewConstanta.toggle()
        }
    }
}

