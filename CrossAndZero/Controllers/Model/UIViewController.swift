//
//  UIView.swift
//  CrossAndZero
//
//  Created by Евгений on 18/02/2019.
//  Copyright © 2019 Evgeniy. All rights reserved.
//

import UIKit

extension UIViewController {
    
    // REVIEW:
    // Здесь нет смысла делать статический метод потому что используется он на свойствах одного экземпляра,
    // лучше переделать в метод объекта
    // Слово settings тут не очень понятно, цвет фона - это не settings а colors, поэтому понятнее будет название setDefaultColors
    // или setDefaultAppearance
    //
    // func setDefaultAppearance() {
    //      self.view ...
    //      self.navigationController ...
    // }
    
    // DONE: - Имправил
    
    // Цвет навигейшен панени
    func setDefaultAppearance(collectionView: UICollectionView?) {
        self.view.backgroundColor = .zumthor
        self.navigationController?.navigationBar.barTintColor = .scampi
        collectionView?.backgroundColor = .silver
    }
}
