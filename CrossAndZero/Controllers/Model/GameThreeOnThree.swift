//
//  GameFreeOnFree.swift
//  CrossAndZero
//
//  Created by Евгений on 14/03/2019.
//  Copyright © 2019 Evgeniy. All rights reserved.
//

import UIKit

class GameFreeOfFree: NumberOfPlayingBox {
    
    var numberOfPlayingBox = 9
    
    var winnCombinations = [[0, 1, 2], [3, 4, 5], [6, 7, 8], [0, 3, 6], [1, 4, 7], [2, 5, 8], [0, 4, 8], [2, 4, 6]]
    
}
