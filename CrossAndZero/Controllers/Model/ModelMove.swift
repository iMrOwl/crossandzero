//
//  Model.swift
//  CrossAndZero
//
//  Created by Евгений on 18/02/2019.
//  Copyright © 2019 Evgeniy. All rights reserved.
//

import UIKit
import Foundation

struct ModelMove {
    
    static var shared = ModelMove()

    var dictionaryImage = Array(repeating: " ", count: 9)
    var defaulDictionaryImage = Array(repeating: " ", count: 9)
    
    var dictionaryState = Array(repeating: true, count: 9)
    var defaulDictionaryState = Array(repeating: true, count: 9)
}
