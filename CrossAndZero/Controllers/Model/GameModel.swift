//
//  DataModel.swift
//  CrossAndZero
//
//  Created by Евгений on 22/02/2019.
//  Copyright © 2019 Evgeniy. All rights reserved.
//

import UIKit

struct GameModel {
    
    static var shared = GameModel()
    
    let returnInitialState = ReturnInitialState()
    let setImage = SetImage()
    let winCheck = WinCheck()
    
    var arrayUsers = [User]()
    
    var userOne = FactoryUsers.sharet.createUsers(nameEnum: .userOne)
    var userTwo = FactoryUsers.sharet.createUsers(nameEnum: .userTwo)

    // REVIEW: Названия очень непонятные, лучше напиши по-русски, но так, чтобы было ясно. Да, в свифте можно писать по-русски =)
    var countCross = 0
    var countZero = 7
    var countMoves = 0
    var staticCount = true
    var move = true
    var winZero = 0
    var winCross = 0
    var goResultViewConstanta = false
}
