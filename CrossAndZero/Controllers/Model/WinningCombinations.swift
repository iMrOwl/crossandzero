//
//  WinningCombinations.swift
//  CrossAndZero
//
//  Created by Евгений on 22/02/2019.
//  Copyright © 2019 Evgeniy. All rights reserved.
//

import UIKit

struct WinnCombinations {
    
    static var shared = WinnCombinations()
    
    let winnCombinations = [[0, 1, 2], [3, 4, 5], [6, 7, 8], [0, 3, 6], [1, 4, 7], [2, 5, 8], [0, 4, 8], [2, 4, 6]]
    
    var arraySaveAllWalk = [Int]()
    var arraySaveUserOneWalk = [Int]()
    var arraySaveUserTwoWalk = [Int]()
}
